CREATE TABLE domain_item
(
  ID            bigint      PRIMARY KEY,
  Information   varchar(255)     NOT NULL
);