package com.sp.pocservice.repository;


import com.sp.pocservice.domain.DomainItem;
import org.springframework.data.repository.CrudRepository;

public interface DomainRepository extends CrudRepository<DomainItem,Long> {

    Iterable<DomainItem> findByIdNotNull();
}
