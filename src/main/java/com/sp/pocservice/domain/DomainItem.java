package com.sp.pocservice.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "DomainItem")
public class DomainItem {

    public DomainItem(Long id, String information) {
        this.id = id;
        this.information = information;
        this.domainData = information;
    }

    @NonNull
    @Id
    @Getter
    private Long id;

    @Column(name = "information")
    private String information;

    @Getter
    @Column(name = "domain_Data")
    private String domainData;

    public void setInformation(String info){
        information = info;
        domainData = info;
    }

    public void setDomainData(String data){
        domainData = data;
        information = data;
    }
}
