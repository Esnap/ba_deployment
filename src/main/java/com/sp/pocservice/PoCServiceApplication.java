package com.sp.pocservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoCServiceApplication {

    public static void main(String[] args) {

        SpringApplication.run(PoCServiceApplication.class, args);
    }

}
